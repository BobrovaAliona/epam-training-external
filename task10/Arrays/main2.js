var arr = [
	[1, 2, 7, -3, 19],
	[-2, 4, -5, 4, 9],
	[1, 7, -4, 6, 1],
	[3, 1, 6, 7, -7],
	[1, 2, 8, 1, -9]
];

for (var i = 0; i < arr.length; i++){
	for (var j = 0; j < arr.length; j++){
		if (i === j && arr[i][j] < 0) {
			arr[i][j] = 0;
		}
		if (i === j && arr[i][j] > 0) {
			arr[i][j] = 1;
		}
	}
}
console.log(arr);